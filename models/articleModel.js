const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var articleSchema = new Schema({
    'marque': {type: String, required: true},
    'modele': {type: String, required: true},
    'prix': {type: Number, required: true},
    "caracteristiques_technique": {type: String, required: true},
    'etat_esthetique': {type: String, required: true},
    'vendeur': {
        type: Schema.Types.ObjectId,
        ref: 'Client' 
    },
    'acheteur': {
        type: Schema.Types.ObjectId,
        ref: 'Client'
    },
    'vendu': {type: String, default: 'non'},
    'createdAt': {type: Date, default:Date.now()}
});

module.exports = mongoose.model('Article', articleSchema);