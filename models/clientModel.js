const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var ClientSchema = new Schema({
    'username': {type: String, required:true, unique: true},
    'firstname': {type: String, required:false},
    'lastname': {type: String, required:false},
    'email': {type: String, required:true, unique: true},
    'password': {type: String, required:true},
    'address': {type: String, required:true},
    'marchand': {type: String, default: 'non'},
    'greenCoin': {type: Number, default: 0},
    'createdAt': {type: Date, default:Date.now()}
});

ClientSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Client', ClientSchema);