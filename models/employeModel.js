const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var EmployeSchema = new Schema({
    'matricule': {type: String, required:true, unique: true},
    'firstname': {type: String, required:false},
    'lastname': {type: String, required:false},
    'password': {type: String, required:true},
    'createdAt': {type: Date, default:Date.now()}
});

EmployeSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Employe', EmployeSchema);