const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var AssociationSchema = new Schema({
    'name': {type: String, required: true},
    'address': {type: String, required: true},
    'rna': {type: String, required: true},
    'email': {type: String, required: true},
    'password': {type: String, required: true},
    'createdAt': {type: Date, default: Date.now()}
});

AssociationSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Association', AssociationSchema);