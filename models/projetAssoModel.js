const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var AssociationSchema = new Schema({
    'name': {type: String, required: true},
    'greencoin': {type: Number, default: 0},
    'description': {type: String, required: true},
    'fin': {type: Date, required: true},
    'accepte': {type: String, default:'non'},
    'association': {
        type: Schema.Types.ObjectId,
        ref: 'Association'
    },
    'createdAt': {type: Date, default: Date.now()}
});

AssociationSchema.plugin(uniqueValidator);

module.exports = mongoose.model('ProjetAssociation', AssociationSchema);