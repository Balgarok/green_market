const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var articleExampleSchema = new Schema({
    'marque': {type: String, required: true},
    'modele': {type: String, required: true},
    'prix': {type: Number},
    'createdAt': {type: Date, default:Date.now()}
});

module.exports = mongoose.model('ArticleExample', articleExampleSchema);