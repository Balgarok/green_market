const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var categorySchema = new Schema({
    'titre': {type: String, required: true},
    'description': {type: String, required: true},
});

module.exports = mongoose.model('Category', categorySchema);