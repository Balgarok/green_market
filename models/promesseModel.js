const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var promesseSchema = new Schema({
    'marque': {type: String, required: true},
    'modele': {type: String, required: true},
    'prix': {type: Number},
    "caracteriques_technique": {type: String, required: true},
    'etat_esthetique': {type: String, required: true},
    'vente': {type: String, default:"non"},
    'etat': {type: String, default:"En attente de prise en charge"},
    'bon': {type: String},
    'userId': {
        type: Schema.Types.ObjectId,
        ref: 'Client'
    },
    'employeId': {
        type: Schema.Types.ObjectId,
        ref: 'Employe'
    },
    'createdAt': {type: Date, default:Date.now()}
});

module.exports = mongoose.model('Promesse', promesseSchema);