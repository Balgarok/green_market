const nodemailer = require('nodemailer');

module.exports = {
    sendResetMail: (req, res, token)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Merci d'aller à cette adresse pour reset votre password "+token;
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: req.body.email,
            subject: "Reset your password",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailModif: (req, res, modif, email)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Modification de votre promesse son état est passé à "+modif;
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Modification de votre promesse",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailRefusEm: (req, res, email)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Votre promesse a été supprimé merci de payer le bon colissimo pour récupérer votre objet.";
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Refus de mis en vente",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailOffre: (req, res, email, bon)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Votre promesse a été accepté voici le numéro de bon colissimo: "+bon;
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Promesse acceptée",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailSuppression: (req, res, email, modele, marque)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Votre promesse pour "+marque+" "+modele+" a été supprimé";
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Promesse supprimée",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailVente: (req, res, email, marque, modele) =>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Votre promesse pour "+marque+" "+modele+" a été mise en vente";
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Promesse mise en vente",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailCharge: (req, res, email, modele, marque)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Votre promesse pour "+marque+" "+modele+" a été prise en charge";
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Promesse prise en charge",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    },
    sendEmailAchat: (req, res, email, modele, marque, prix)=>{
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: 'balgarok75@gmail.com',
                pass: 'SaulTarvitz'
            }
        });

        var message = "<br>Message : Vous venez d'acheter "+marque+" "+modele+" pour "+prix;
        var mailOptions = {
            from: 'balgarok75@gmail.com',
            to: email,
            subject: "Promesse prise en charge",
            html: message
        };

        transporter.sendMail(mailOptions, (err, infos)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err
                });
            }
        });
    }
}