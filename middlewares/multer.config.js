const multer = require('multer');

const MIME_TYPE = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/png': 'png'
}

const storage = multer.diskStorage({
    destination: (req, file, callback)=>{
        callback(null, 'public/images/promesses');
    },
    filename: (req, file, callback)=>{
        var name = Math.floor(Math.random() * Math.floor(15258652325)).toString();
        name += Math.floor(Math.random() * Math.floor(15258652328555)).toString();
        name += Math.floor(Math.random() * Math.floor(15256552325)).toString();
        name += Math.floor(Math.random() * Math.floor(1525865854225)).toString();
        name+= Date.now()+'.';
        const extension = MIME_TYPE[file.mimetype];
        name += extension;

        callback(null, name);
    }
});

module.exports = multer({storage}).single('image');