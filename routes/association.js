var express = require('express');
var router = express.Router();
const projetAssoController = require('../controllers/projetAssoController');

router.get('/listAsso', projetAssoController.getAssoList);

router.get('/assoProjet/:id', projetAssoController.listProjet);

router.get('/projet/:id', projetAssoController.show);

router.post('/projet', projetAssoController.create);

router.delete('/projet/:id', projetAssoController.suppression);

router.put('/projet/:id', projetAssoController.accepte);

router.put('/projet/coin/:id', projetAssoController.ajoutCoin);

router.get('/projetDemande/:id', projetAssoController.listDemande);

module.exports = router;