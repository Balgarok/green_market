var express = require('express');
const authEm = require('../middlewares/authEm');
const exampleController = require('../controllers/exampleController');
var router = express.Router();

router.post('/', authEm, exampleController.ajout);

router.get('/liste', authEm, exampleController.list);

router.get('/:id', authEm, exampleController.show);

router.delete('/:id', authEm, exampleController.suppression);

module.exports = router;