var express = require('express');
const authEm = require('../middlewares/authEm');
const auth = require('../middlewares/auth');
const promesseController = require('../controllers/promesseController');
var router = express.Router();

router.get('/:id', auth, promesseController.show);

router.get('/employeVue/:id', authEm, promesseController.show);

router.get('/employe/:id', authEm, promesseController.show);

router.get('/', authEm, promesseController.list);

router.post('/', auth, promesseController.creation);

router.put('/offre/:id', auth, promesseController.offre);

router.put('/:id', authEm, promesseController.modification);

router.put('/accepte/:id', auth, promesseController.vente);

router.delete('/:id', auth, promesseController.suppression);

router.put('/refusEm', authEm, promesseController.refusEm);

router.get('/listesuivi/:id', authEm, promesseController.suiviList);

router.get('/userlist/:id', auth, promesseController.userList);

router.put('/charge/:id', authEm, promesseController.charge);

router.get('/stat', authEm, promesseController.stat);

module.exports = router;