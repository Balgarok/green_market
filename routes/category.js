var express = require('express');
const authEm = require('../middlewares/authEm');
const categoryController = require('../controllers/categoryController');
const auth = require('../middlewares/auth');
var router = express.Router();

router.post('/', authEm, categoryController.ajout);

router.delete('/:id', authEm, categoryController.suppression);

router.get('/liste', authEm, categoryController.liste);

router.get('/:id', authEm, categoryController.show);

router.get('/crea', auth, categoryController.listCrea);

module.exports = router;