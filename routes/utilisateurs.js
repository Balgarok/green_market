var express = require('express');
var userController = require('../controllers/userController');
var auth = require('../middlewares/auth');
var authEm = require('../middlewares/authEm');
var router = express.Router();

router.post('/signup', userController.signup);

router.post('/signupEm', userController.signupEm);

router.post('/signupAsso', userController.signupAsso);

router.post('/login', userController.login);

router.post('/loginEm', userController.loginEm);

router.post('/loginAsso', userController.loginAsso);

router.put('/update', auth, userController.update);

router.get('/infos/:id', auth, userController.infos);

router.put('/marchand/:id', auth, userController.marchand);

router.get('/demandeMarchand', authEm, userController.demandeMarchand);

router.put('/marchandOk/:id', authEm, userController.marchandOk);

module.exports = router;