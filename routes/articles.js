var express = require('express');
const articleController = require('../controllers/articleController');
const auth = require('../middlewares/auth');
const authEm = require('../middlewares/authEm');
var router = express.Router();

router.get('/:id', articleController.show);

router.get('/', articleController.list);

router.put('/:id', auth, articleController.achat);

router.get('/userlist/:id', auth, articleController.userList);

router.get('/vendu', authEm, articleController.vendu);

router.get('/ventes/:id', auth, articleController.ventes);

router.delete('/:id', authEm, articleController.suppression);

module.exports = router;