const promesseService = require('../service/promesseService');
const exampleService = require('../service/exampleService');

module.exports = {
    creation: async (req, res)=>{
        await exampleService.recherche(req, res);
    },
    show: async (req, res)=>{
        await promesseService.show(req, res);
    },
    list: async (req, res)=>{
        await promesseService.list(req, res);
    },
    offre: async (req ,res)=>{
        await promesseService.offre(req, res);
    },
    refusEm: async (req ,res)=>{
        await promesseService.refusEm(req, res);
    },
    modification: async (req, res)=>{
        await promesseService.modification(req, res);
    },
    vente: async (req, res)=>{
        await promesseService.vente(req, res);
    },
    suppression: async (req, res)=>{
        await promesseService.suppression(req, res);
    },
    userList: async (req, res)=>{
        await promesseService.userList(req, res);
    },
    suiviList: async (req, res)=>{
        await promesseService.suiviList(req, res);
    },
    charge: async (req, res)=>{
        await promesseService.charge(req, res);
    },
    stat: async (req, res)=>{
        await promesseService.stat(req, res);
    }
}