const articleService = require('../service/articleService');
const clientService = require('../service/clientService');

module.exports = {
    list: async (req, res)=>{
        await articleService.list(req, res);
    },
    show: async (req, res)=>{
        await articleService.show(req ,res);
    },
    achat: async (req, res)=>{
        await clientService.green(req);
        await articleService.achat(req, res);
        
    },
    userList: async (req, res)=>{
        await articleService.userlist(req, res); 
    },
    vendu: async (req, res)=>{
        await articleService.vendu(req, res);
    },
    ventes: async (req, res)=>{
        await articleService.ventes(req, res);
    },
    suppression: async (req, res)=>{
        await articleService.suppression(req, res);
    }
}