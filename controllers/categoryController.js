const categoryService = require('../service/categoryService');

module.exports = {
    ajout : async (req, res) =>{
        await categoryService.ajout(req, res);
    },
    suppression : async (req, res) =>{
        await categoryService.suppression(req, res);
    },
    liste: async (req, res)=>{
        await categoryService.liste(req, res);
    },
    show: async (req, res)=>{
        await categoryService.show(req, res);
    },
    listCrea: async (req, res)=>{
        await categoryService.listCrea(req, res);
    }
}