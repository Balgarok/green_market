const projetAssoService = require('../service/projetAssoService');
const assoService = require('../service/associationService');
const clientService = require('../service/clientService')

module.exports = {
    create: async (req, res)=>{
        await projetAssoService.create(req, res);
    },
    show: async (req, res)=>{
        await projetAssoService.show(req, res);
    },
    suppression: async (req, res)=>{
        await projetAssoService.suppression(req, res);
    },
    accepte: async (req, res)=>{
        await projetAssoService.accepte(req, res);
    },
    ajoutCoin: async (req, res) =>{
        await projetAssoService.ajoutCoin(req, res);
        await clientService.minusCoin(req, res);
    },
    getAssoList: async (req, res)=>{
        await assoService.listAsso(req, res);
    },
    listProjet: async (req, res)=>{
        await projetAssoService.list(req, res);
    },
    listDemande: async (req, res)=>{
        await projetAssoService.listDemande(req, res);
    }
}