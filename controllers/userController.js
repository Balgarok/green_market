const clientService = require('../service/clientService');
const employeService = require('../service/employeService');
const associationService = require('../service/associationService');

module.exports = {
    signup: async (req, res)=>{
        await clientService.signup(req, res);
    },
    login: async (req, res)=>{
        await clientService.login(req, res);
    },
    update: async (req, res)=>{
        await clientService.update(req, res);
    },
    infos: async (req, res)=>{
        await clientService.infos(req, res);
    },
    signupEm: async (req, res)=>{
        await employeService.signup(req, res);
    },
    loginEm: async (req, res)=>{
        await employeService.login(req, res);
    },
    marchand: async (req, res)=>{
        await clientService.marchand(req, res);
    },
    demandeMarchand: async (req, res)=>{
        await clientService.demandeMarchand(req, res);
    },
    marchandOk: async (req, res)=>{
        await clientService.marchandOk(req, res);
    },
    signupAsso: async (req ,res)=>{
        await associationService.signup(req, res);
    },
    loginAsso: async (req ,res)=>{
        await associationService.login(req, res);
    }
}