const exampleService = require('../service/exampleService');

module.exports = {
    ajout: async (req, res)=>{
        await exampleService.ajout(req, res);
    },
    suppression: async (req, res)=>{
        await exampleService.suppression(req, res);
    },
    list: async (req, res)=>{
        await exampleService.list(req, res)
    },
    show: async (req, res)=>{
        await exampleService.show(req, res)
    }
}