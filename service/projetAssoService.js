const projetAssoModel = require('../models/projetAssoModel');
const clientService = require('../service/clientService');

module.exports = {
    create: async (req, res)=>{
        const id = req.params.userId
        var projet = new projetAssoModel({
            name: req.body.name,
            description: req.body.description,
            association: req.params.id,
            fin: req.body.fin
        });
        projet.save((err, projet)=>{
            if(err){
                console.log(err.message);
                return res.status(500).json({
                    status: 500,
                    message: 'Error when creating promesse'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Promesse created'
            });
        })
    },
    show: async (req, res)=>{
        const id = req.params.id;
        projetAssoModel.findOne({_id: id}, (err, projet)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting projet'
                }); 
            }
            if(!projet){
                return res.status(404).json({
                    status: 404,
                    message: 'Projet not found'
                });
            }
            return res.status(200).json({
                status:200,
                result: projet
            });
        });
    },
    suppression: async (req, res)=>{
        const id = req.params.id;
            await projetAssoModel.findByIdAndRemove(id, (err, projet)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when deleting projet'
                    }); 
                }
                if(!projet){
                    return res.status(404).json({
                        status: 404,
                        message: 'Projet to remove not found'
                    });
                }
                return res.status(204).json();
            });
    },
    accepte: async (req, res)=>{
        const id = req.params.id;
        projetAssoModel.findOne({_id: id}, (err, projet)=>{
            
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error while retrieving projet'
                });
            }
            if(!projet){
                return res.status(404).json({
                    status: 404,
                    message: 'Promesse not found for transforming into projet'
                });
            }
            projet.accepte = 'oui';
            projet.updateOne({accepte:'oui'},(err, projet)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: "Issue with transforming promesse into projet"
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: "Projet accepté"
                });
            });
        
        });
    },
    ajoutCoin: async (req, res)=>{
        const id = req.params.id;
        projetAssoModel.findOne({_id: id}, (err, projet)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error while retrieving projet'
                });
            }
            if(!projet){
                return res.status(404).json({
                    status: 404,
                    message: 'Projet not found'
                });
            }
            const newCoin = parseInt(projet.greencoin) + parseInt(req.body.coin);
            console.log(req.body.coin);
            console.log(newCoin);
            projet.updateOne({greencoin: newCoin},(err, projet)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: "Issue while updating projet"
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: "Projet credite"
                });
            });
            
        });
    },
    list: async (req, res)=>{
        const id = req.params.id;
        projetAssoModel.find(({association: id, accepte: 'oui'}),(err, projets)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: projetAssoModel
            })
        });
    },
    listDemande: async (req, res)=>{
        const id = req.params.id;
        projetAssoModel.find(({association: id, accepte: 'non'}),(err, projets)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: projetAssoModel
            })
        });
    }
}