const employeModel = require('../models/employeModel');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
    signup: async (req, res)=>{
        bcrypt.hash(req.body.password, 10, (err, hash)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }else{
                const newEmploye = new employeModel({
                    matricule: req.body.matricule,
                    firstname: req.body.firstname,
                    lastname: req.body.lastename,
                    password: hash,
                });
                newEmploye.save((err, employe)=>{
                    if(err){
                        return res.status(400).json({
                            status: 400,
                            message: err.message
                        });
                    }
                    return res.status(201).json({
                        status: 201,
                        message: 'Employe created'
                    });
                });
            }
        });
    },
    login: async (req, res)=>{
        employeModel.findOne({matricule: req.body.matricule}, (err, employe)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!employe){
                return res.status(404).json({
                    status: 404,
                    message: 'User not found'
                });
            }
            bcrypt.compare(req.body.password, employe.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }

                return res.status(200).json({
                    userId: employe._id,
                    token: jsonwebtoken.sign(
                        {userId: employe._id},
                        process.env.SECRETEM,
                        {expiresIn: '24h'}
                    ),
                    type: "employe"
                });
            });
        });
    }
}