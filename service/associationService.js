const associationModel = require('../models/associationModel');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');
var request = require('request');
var url = 'https://entreprise.data.gouv.fr/api/rna/v1/id/';



module.exports = {
    signup : async (req, res)=>{
        console.log(req.body.rna);
        request.get({
            url: url+req.body.rna,
            json: true,
            headers: {'User-Agent': 'request'}
          }, (err, resp, data) => {
            if (err) {
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            } else if (res.statusCode !== 200) {
                return res.status(400).json({
                    status: 400,
                    message: err.message
                });;
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash)=>{
                    if(err){
                        return res.status(500).json({
                            status: 500,
                            message: err.message
                        });
                    }
                    else{
                        const newAsso = new associationModel({
                            name: req.body.name,
                            email: req.body.email,
                            address: req.body.address,
                            rna: req.body.rna,
                            password: hash,
                        });
                        newAsso.save((err, asso)=>{
                            if(err){
                                return res.status(400).json({
                                    status: 400,
                                    message: err.message
                                });
                            }
                            return res.status(201).json({
                                status: 201,
                                message: 'Association created'
                            });
                        })
                    }
                });
            }
        });
       
    },
    loginAsso : async (req, res)=>{
        associationModel.findOne({name: req.body.name}, (err, asso)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!asso){
                return res.status(404).json({
                    status: 404,
                    message: 'Association not found'
                });
            }
            bcrypt.compare(req.body.password, asso.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }
                    return res.status(200).json({
                        userId: asso._id,
                        token: jsonwebtoken.sign(
                            {userId: asso._id},
                            process.env.SECRETASSO,
                            {expiresIn: '24h'}
                        ),
                        type: "association"
                    });
            });
        });
    },
    listAsso: async (req, res)=>{
        associationModel.find((err, associations)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error retrieving associations'
                });
            }
            if(!associations){
                return res.status(404).json({
                    status: 404,
                    message: 'There is no associations'
                });
            }
            return res.status(200).json({
                status:200,
                result: associations
            });
        });
    }
}