const categoryModel = require('../models/categoryModel');

module.exports = {
    ajout : async (req, res)=>{
        var newCategory = new categoryModel({
            titre: req.body.titre,
            description: req.body.description
        });
        await newCategory.save((err, category)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when creating category'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Category created'
            });
        })
    },
    liste: async (req, res)=>{
        categoryModel.find((err, categories)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example articles'
                });
            }
            return res.status(200).json({
                status: 200,
                result: categories
            })
        });
    },
    show: async (req, res)=>{
        const id = req.params.id;
        categoryModel.findOne({_id: id},(err, categories)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example articles'
                });
            }
            return res.status(200).json({
                status: 200,
                result: categories
            })
        });
    },
    listCrea: async (req, res)=>{
        categoryModel.find((err, categories)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example articles'
                });
            }
            return res.status(200).json({
                status: 200,
                result: categories
            })
        });
    }
}