
const articleModel = require('../models/articleModel');


module.exports = {
    creation: (promesse, res)=>{
        const article = new articleModel({
            marque: promesse.marque,
            modele: promesse.modele,
            prix: promesse.prix,
            vendeur: promesse.userId,
            caracteristiques_technique: promesse.caracteriques_technique,
            etat_esthetique: promesse.etat_esthetique
        });
        console.log("entre");
        article.save((err, article)=>{
            if(err){
                console.log(err);
                return res.status(500).json({
                    status: 500,
                    message: 'Error when creating article'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Article created'
            });
        });
    },
    show: async (req, res)=>{
        const id = req.params.id;
        articleModel.findOne({_id: id}, (err, article)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving article'
                });
            }
            if(!article){
                return res.status(404).json({
                    status: 404,
                    message: 'Article not found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: article
            });
        });
    },
    list: async (req, res)=>{
        articleModel.find(({vendu:'non'}),(err, articles)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all articles'
                });
            }
            if(!articles){
                return res.status(404).json({
                    status: 404,
                    message: 'No articles found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: articles
            });
        });
    },
    achat: async (req, res)=>{
        const id = req.params.id;
        articleModel.findOne({_id: id}, (err, article)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when suppressin article'
                });
            }
            if(!article){
                return res.status(404).json({
                    status: 404,
                    message: 'Article not found'
                });
            }
            article.vendu = req.body.vente ? req.body.vente : article.vendu;
            article.acheteur = req.body.userId ? req.body.userId : article.acheteur;
            article.updateOne({vendu:req.body.vente, acheteur:req.body.userId},(err, article)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Issue while achat'
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'Article vendu'
                });
            });
        });
    },
    userlist: async (req, res)=>{
        const id = req.params.id
        articleModel.find(({vendu:'oui', acheteur: id}),(err, articles)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all articles'
                });
            }
            if(!articles){
                return res.status(404).json({
                    status: 404,
                    message: 'No articles found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: articles
            });
        });
    },
    vendu: async (req, res)=>{
        articleModel.find(({vendu:'oui'}),(err, articles)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all articles'
                });
            }
            if(!articles){
                return res.status(404).json({
                    status: 404,
                    message: 'No articles found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: articles
            });
        });
    },
    ventes: async (req, res)=>{
        const id = req.params.id
        articleModel.find(({vendu:'non', vendeur: id}),(err, articles)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all articles'
                });
            }
            if(!articles){
                return res.status(404).json({
                    status: 404,
                    message: 'No articles found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: articles
            });
        });
    },
    suppression: async (req, res)=>{
        const id = req.params.id;
        articleModel.findByIdAndRemove(id, (err, article)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when deleting product'
                }); 
            }
            if(!article){
                return res.status(404).json({
                    status: 404,
                    message: 'Product to remove not found'
                });
            }
            return res.status(204).json();    
        });
    }
}