const exampleModel = require('../models/articleExampleModel');
const promesseService = require('../service/promesseService');

module.exports = {
    ajout: async (req, res)=>{
        var example = new exampleModel({
            marque: req.body.marque,
            modele: req.body.modele,
            prix: req.body.prix
        });
        await example.save((err, example)=>{
            if(err){
                return res.status(400).json({
                    status: 500,
                    message: 'Error when creating example'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Example created'
            })
        })
    },
    suppression: async (req, res)=>{
        const id = req.params.id;
        await articleExample.findByIdAndRemove(id, (err, example)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when deleting example'
                }); 
            }
            if(!promesse){
                return res.status(404).json({
                    status: 404,
                    message: 'Example to remove not found'
                });
            }
            return res.status(204).json();    
        });
    },
    recherche: async (req, res)=>{
        exampleModel.findOne({marque: req.body.marque, modele: req.body.modele}, (err, example)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example'
                });
            }
            if(example===null){
                exampleModel.findOne({marque: req.body.marque}, (err, exampleP)=>{
                    if(err){
                        return res.status(500).json({
                            status: 500,
                            message: 'Error when getting example'
                        });
                    }
                    return promesseService.creation(req, res, exampleP.prix) ;
                });
            }
            return promesseService.creation(req, res, example.prix);
        });
    },
    list: async (req, res)=>{
        exampleModel.find((err, examples)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example articles'
                });
            }
            return res.status(200).json({
                status: 200,
                result: examples
            })
        });
    },
    show: async (req, res)=>{
        const id = req.params.id
        exampleModel.findOne({_id: id}, (err, example)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting example article'
                });
            }
            return res.status(200).json({
                status: 200,
                result: example
            })
        });
    }
}