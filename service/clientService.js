const clientModel = require('../models/clientModel');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
    signup: async (req, res)=>{
        bcrypt.hash(req.body.password, 10, (err, hash)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            else{
                const newClient = new clientModel({
                    username: req.body.username,
                    firstname: req.body.firstname,
                    lastname: req.body.lastename,
                    email: req.body.email,
                    address: req.body.address,
                    password: hash,
                });
                newClient.save((err, client)=>{
                    if(err){
                        return res.status(400).json({
                            status: 400,
                            message: err.message
                        });
                    }
                    return res.status(201).json({
                        status: 201,
                        message: 'Client created'
                    });
                })
            }
        });
    },
    login: async (req, res) =>{
        clientModel.findOne({username: req.body.username}, (err, client)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!client){
                return res.status(404).json({
                    status: 404,
                    message: 'User not found'
                });
            }
            bcrypt.compare(req.body.password, client.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }
                if(client.marchand=='non'||client.marchand=='demande'){
                    return res.status(200).json({
                        userId: client._id,
                        token: jsonwebtoken.sign(
                            {userId: client._id},
                            process.env.SECRET,
                            {expiresIn: '24h'}
                        ),
                        type: "client"
                    });
                }else{
                    return res.status(200).json({
                        userId: client._id,
                        token: jsonwebtoken.sign(
                            {userId: client._id},
                            process.env.SECRET,
                            {expiresIn: '24h'}
                        ),
                        type: "marchand"
                    });
                }
            });
        });
    },
    update: async (req, res)=>{
        clientModel.findOne({_id: req.body.userId}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'User not found'
                });
            }
            user.username = req.body.usernamne ? req.body.username : user.username;
            user.firstname = req.body.firstname ? req.body.firstname : user.firstname;
            user.lastname = req.body.lastname ? req.body.lastname : user.lastname;
            user.username = req.body.username ? req.body.username : user.username;

            user.save((err, user)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error while modifying user profile'
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'User profile modified'
                });
            });
        });
    },
    green: async (req)  =>{
        const id = req.body.userId;
        let coin = req.body.prix/10;
        console.log(coin);
        clientModel.findOne({_id: id}, (err, user)=>{
            if(err){
                console.log(err);
            }
            if(!user){
                console.log('no user');
            }
            coin = user.greenCoin + coin;
            console.log(coin);
            user.updateOne({greenCoin: coin},(err, user)=>{
                if(err){
                    console.log(err.message);
                }
            });
        });
    },
    infos: async (req, res)=>{
        const id = req.params.id;
        clientModel.findOne({_id: id}, (err, client)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving client'
                });
            }
            if(!client){
                return res.status(404).json({
                    status: 404,
                    message: 'Client not found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: client
            });
        });
    },
    marchand: async (req, res)=>{
        const id = req.params.id;
        clientModel.findOne({_id: id}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving client'
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'Client not found'
                });
            }
            user.updateOne({marchand: 'demande'},(err, user)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when retrieving client'
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'User profile modified'
                });
            });
        });
    },
    demandeMarchand: async (req, res)=>{
        clientModel.find({marchand: 'demande'}, (err, users)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving client'
                });
            }
            if(!users){
                return res.status(404).json({
                    status: 404,
                    message: 'Client not found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: users
            });    
        });
    },
    marchandOk: async (req, res)=>{
        const id = req.params.id;
        clientModel.findOne({_id: id}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving client'
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'Client not found'
                });
            }
            user.updateOne({marchand: 'oui'},(err, user)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when retrieving client'
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'User profile modified'
                });
            });
        });
    },
    minusCoin: async (req, res)=>{
        console.log("entrée");
        const id = req.body.userId;
        const minus = req.body.coin;
        clientModel.findOne({_id: id}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving client'
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'Client not found'
                });
            }
            const newCoin = user.greenCoin - minus;
            user.updateOne({greenCoin : newCoin}, (err, user)=>{
                if(err){
                    console.log(err.message);
                }
            });
        });
        
    }
}

