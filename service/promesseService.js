const promesseModel = require('../models/promesseModel');
const articleService = require('../service/articleService.js');

module.exports = {
    creation: (req, res, prix) =>{
        var promesse = new promesseModel({
            marque: req.body.marque,
            modele: req.body.modele,
            prix: prix,
            userId: req.body.userId,
            caracteriques_technique: req.body.caracteristique_technique,
            etat_esthetique: req.body.etat_esthetique
        });
        promesse.save((err, promesse)=>{
            if(err){
                console.log(err.message);
                return res.status(500).json({
                    status: 500,
                    message: 'Error when creating promesse'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Promesse created'
            });
        })
        
    },
    show: async (req, res) =>{
        const id = req.params.id;
        promesseModel.findOne({_id: id}, (err, promesse)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesse'
                }); 
            }
            if(!promesse){
                return res.status(404).json({
                    status: 404,
                    message: 'Promesse not found'
                });
            }
            return res.status(200).json({
                status:200,
                result: promesse
            });
        });
    },
    list: async (req, res) =>{
        promesseModel.find(({etat: 'En attente de prise en charge'}),(err, promesses)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: promesses
            })
        });
    },
    offre: async (req, res)=>{
        var bon = "1L"
        bon += (Math.floor(Math.random()*Math.floor(100000000000))).toString()
        const id = req.params.id;
        promesseModel.updateOne({_id: id}, {etat: 'offre de rachat accepte', bon: bon}, (err, data)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when updating promesse',
                    error: err
                }); 
            }
            return res.status(200).json({
                status: 200,
                message: 'Promesse updated'
            });
        });
    },
    refusEm: async (req, res)=>{
        const id = req.params.id;
        await promesseModel.updateOne({_id: id}, {etat: 'Refus de contre Offre'}, (err, data)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when updating promesse',
                    error: err
                }); 
            }
            return res.status(200).json({
                status: 200,
                message: 'Promesse updated'
            });
        });
    },
    modification:async (req, res) =>{
        const id = req.params.id;
        promesseModel.findOne({_id: id}, (err, promesse)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when updating promesse',
                    error: err
                });
            }
            if(!promesse){
                return res.status(404).json({
                    status: 404,
                    message: 'Promesse not found',
                    error: err
                });
            }
            promesse.prix = req.body.prix ? req.body.prix : promesse.prix;
            promesse.caracteristiques_technique = req.body.caracteristiques_technique ? req.body.caracteristiques_technique : promesse.caracteristiques_technique;
            promesse.etat_esthetique = req.body.etat_esthetique ? req.body.etat_esthetique : promesse.etat_esthetique;
            promesse.etat = req.body.etat ? req.body.etat : promesse.etat;
            promesseModel.updateOne({_id: id},{prix: req.body.prix, caracteriques_technique: req.body.caracteristique_technique, etat_esthetique: req.body.etat_esthetique, etat: req.body.etat},(err, data)=>{
                if(err){
                    console.log(err);
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when updating promesse',
                        error: err
                    }); 
                }
                return res.status(200).json({
                    status: 200,
                    message: 'Promesse updated'
                });
            });
        });
    },
    suppression: async (req, res) =>{
        const id = req.params.id;
            await promesseModel.findByIdAndRemove(id, (err, promesse)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when deleting promesse'
                    }); 
                }
                if(!promesse){
                    return res.status(404).json({
                        status: 404,
                        message: 'Promesse to remove not found'
                    });
                }
                return res.status(204).json();
            });
    },
    vente: async (req, res)=>{ 
            promesseModel.findOne({_id: req.params.id}, (err, promesse)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error while retrieving promesse'
                    });
                }
                if(!promesse){
                    return res.status(404).json({
                        status: 404,
                        message: 'Promesse not found for transforming into article'
                    });
                }
                promesse.vente = 'oui';
                promesse.updateOne({vente: 'oui'},(err, promesse)=>{
                    if(err){
                        return res.status(500).json({
                            status: 500,
                            message: "Issue with transforming promesse into article"
                        });
                    }
                });
                articleService.creation(promesse, res);
            });
    },
    charge: async (req, res)=>{
        var prise = "promesse prise en charge";
        const id = req.params.id;
        promesseModel.updateOne({_id: id}, {etat: prise, employeId: req.body.employe}, (err, data)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when prise en charge',
                    error: err
                });
            }
            return res.status(200).json({
                status: 200,
                message: 'Prise en charge effectuee'
            });
        })
    },
    userList: async (req, res)=>{
        const id = req.params.id
        promesseModel.find(({userId: id, vente:'non'}),(err, promesses)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: promesses
            })
        });
    },
    suiviList: async (req, res)=>{
        const id = req.params.id;
        promesseModel.find(({employeId: id, vente:'non'}),(err, promesses)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: promesses
            })
        });
    },
    stat: async (req, res)=>{
        await promesseModel.find(({vente: 'oui'}),(err, promesses)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when getting promesses'
                });
            }
            return res.status(200).json({
                status: 200,
                result: promesses
            })
        });
    }
}
